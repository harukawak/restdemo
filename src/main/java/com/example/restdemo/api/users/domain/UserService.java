package com.example.restdemo.api.users.domain;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    public List<User> selectAllUsers() {

        // TODO ダミーデータをaddして返す
        List<User> userList = new ArrayList<>();

        User user1 = new User(1, "user1");
        userList.add(user1);
        User user2 = new User(2, "user2");
        userList.add(user2);

        return userList;
    }

    public User create(User user) {
        System.out.println(
                "user created! " + "id: " + user.getId() + "name: " + user.getName());
        return user;
    }

    public User update(User user) {
        System.out.println(
                "user updated! " + "id: " + user.getId() + "name: " + user.getName());
        return user;
    }

    public int delete(int id) {
        System.out.println("user deleted! " + "id: " + id);
        final int OK = 1;
        return OK;
    }
}
