package com.example.restdemo.api.users;

import com.example.restdemo.api.users.domain.User;
import com.example.restdemo.api.users.domain.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/demo/api/v1/users")
public class UsersRestController {

    @Autowired
    UserService userService;

    @GetMapping("")
    public List<User> getUsersList() {

        return userService.selectAllUsers();
    }


    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        return userService.create(user);
    }

    @PutMapping("")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public User updateUser(@RequestBody User user) {
        return userService.update(user);
    }

    @DeleteMapping("{id:.+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String daleteUser(@PathVariable("id") int id) {
        int result = userService.delete(id);
        if (result == 1) {
            return "deleted.";
        } else {
            return "failure.";
        }
    }

}
