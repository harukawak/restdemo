package com.example.restdemo.api.users.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class User {

    int id;

    String name;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public User() {

    }
}
